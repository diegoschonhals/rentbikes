﻿using System;
using BusinessRules.Classes;
using BusinessRules.Exceptions;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    [TestClass]
    public class ClientTests
    {
        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void Client_InvalidParameters_throws_exception()
        {
            Client c = new Client(0, "", "", "3446-426943", "primera junta 7", "18817829", "dschonhals@hotmail.com");
        }

        [TestMethod]
        public void Client_TestConstructor_ValidParameters()
        {
            Client c = new Client(1, "Diego", "Schonhals", "3446-426943", "primera junta 7", "18817829", "dschonhals@hotmail.com");

            Assert.IsTrue(c.Id == 1);
            Assert.AreEqual(c.FirstName, "Diego");
            Assert.AreEqual(c.LastName, "Schonhals");
            Assert.AreEqual(c.Phone, "3446-426943");
            Assert.AreEqual(c.Address, "primera junta 7");
            Assert.AreEqual(c.DocumentId, "18817829");
            Assert.AreEqual(c.Email, "dschonhals@hotmail.com");

        }



        [TestMethod]
        public void Client_GetOneByID_NotFound_ReturnNull()
        {
            Client c1 = new Client().GetOneById(100);
            Assert.IsTrue(c1 == null);
        }

        [TestMethod]
        public void Client_GetOneByID_Found_ReturnClient()
        {
            Client c1 = new Client().GetOneById(1);
            Assert.IsInstanceOfType(c1, typeof(Client));
        }

        [TestMethod]
        public void Client_GetAll_ReturnListClients()
        {
            List<Client> Clients = new Client().GetAll();
            Assert.IsNotNull(Clients);
            Assert.IsInstanceOfType(Clients, typeof(List<Client>));
            Assert.IsTrue(Clients.Count > 0);

        }
    }
}
