﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessRules.Exceptions;
using BusinessRules.Classes;

namespace BusinessRules.Tests
{
    [TestClass]
    public class RentedBikesTests
    {
        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void RentedBike_InvalidParameters_throws_exception()
        {
            //wrong parameters: Id, BikeId, ClientId, RentTypeId, price
            //good parameters: StartDate,EndDate, price
            RentedBike RentedBike = new RentedBike(0, 0, 0, 0, DateTime.Now, DateTime.Now.AddDays(1), 0, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void RentedBike_InvalidParametersButDates_throws_exception()
        {
            //good parameters: Id, BikeId, ClientId, RentTypeId, price
            //wrong parameters: StartDate,EndDate
            RentedBike RentedBike = new RentedBike(1, 1, 1, 1, DateTime.Now, DateTime.Now.AddDays(-1), 50, null);
        }

        [TestMethod]
        public void RentedBike_TestConstructor_ValidParameters()
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now.AddDays(1);


            RentedBike rentedBike = new RentedBike(1, 3, 2, 3, startDate, endDate, 50, 1);
            Assert.AreEqual(rentedBike.Id, 1);
            Assert.AreEqual(rentedBike.BikeId, 3);
            Assert.AreEqual(rentedBike.ClientId, 2);
            Assert.AreEqual(rentedBike.RentTypeId, 3);
            Assert.AreEqual(rentedBike.StartDate, startDate);
            Assert.AreEqual(rentedBike.EndDate, endDate);
            Assert.IsTrue(rentedBike.EndDate > rentedBike.StartDate);

        }



        [TestMethod]
        public void RentedBike_IsBikeRented_ReturnBoolean()
        {
            //return true, is bike is rented

            RentedBike rb = new RentedBike();

            //it should be rented, because startdate is between startdate and endDate, and endDate (one hour more) is smaller than endDate
            bool returnTrue = new RentedBike().IsBikeRented(1, DateTime.Now, DateTime.Now.AddHours(1), rb.GetAll());
            Assert.IsTrue(true == returnTrue);

            //return false, if bike is not rented
            bool returnFalse = new RentedBike().IsBikeRented(1, DateTime.Now.AddDays(3), DateTime.Now.AddDays(4), rb.GetAll());
            Assert.IsTrue(false == returnFalse);

        }

        [TestMethod]
        public void RentedBike_GetNextIdValue_ReturnValueGreaterThan0()
        {
            RentedBike rb = new RentedBike();
            int id = rb.GetNextIdValue(rb.GetAll());
            Assert.IsTrue(id > 0);
        }

        [TestMethod]
        public void RentedBike_RentBike_AddOneBike_ReturnBoolean()
        {
            RentedBike rentedBike = new RentedBike();
            //should have 4 items
            List<RentedBike> rentedBikes = rentedBike.GetAll();
            int oldRentedBikes = rentedBikes.Count;

            bool returnValue = rentedBike.RentBike(1, 4, 1, DateTime.Now, DateTime.Now.AddDays(1), null, ref rentedBikes);


            int currentRentedBikes = rentedBikes.Count;

            Assert.IsTrue(returnValue==true);
            Assert.IsTrue(currentRentedBikes > oldRentedBikes);
        }


        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void RentedBike_RentBike_AddOneBike_WrongParameters_ThrowsException()
        {
            RentedBike rentedBike = new RentedBike();
            //should have 4 items
            List<RentedBike> rentedBikes = rentedBike.GetAll();
            bool returnValue = rentedBike.RentBike(-1, -4, -1, DateTime.Now, DateTime.Now.AddDays(1), null, ref rentedBikes);
        }

        [TestMethod]
        public void RentedBike_RentBike_AddOneBike_WrongDates_ReturnFalse()
        {
            RentedBike rentedBike = new RentedBike();
            //should have 4 items
            List<RentedBike> rentedBikes = rentedBike.GetAll();
            int oldRentedBikes = rentedBikes.Count;

            bool returnValue = rentedBike.RentBike(1, 1, 1, DateTime.Now, DateTime.Now.AddDays(1), null, ref rentedBikes);

            int currentRentedBikes = rentedBikes.Count;

            Assert.IsTrue(returnValue == false);
            Assert.IsTrue(currentRentedBikes == oldRentedBikes);
        }

        [TestMethod]
        public void RentedBike_RentBike_AddOneBike_WithPromotion_ReturnTrue()
        {
            RentedBike rentedBike = new RentedBike();
            //should have 4 items
            List<RentedBike> rentedBikes = rentedBike.GetAll();
            int oldRentedBikes = rentedBikes.Count;

            bool returnValue = rentedBike.RentBike(1, 3, 1, DateTime.Now, DateTime.Now.AddDays(1), 1, ref rentedBikes);

            int currentRentedBikes = rentedBikes.Count;

            Assert.IsTrue(returnValue == true);
            Assert.IsTrue(currentRentedBikes > oldRentedBikes);
        }




        [TestMethod]
        public void RentedBike_GetAll_ReturnListRentedBikes()
        {
            List<RentedBike> RentedBikes = new RentedBike().GetAll();
            Assert.IsNotNull(RentedBikes);
            Assert.IsInstanceOfType(RentedBikes, typeof(List<RentedBike>));
            Assert.IsTrue(RentedBikes.Count > 0);

        }
    }
}
