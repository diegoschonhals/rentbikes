﻿using System;
using BusinessRules.Classes;
using BusinessRules.Exceptions;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    [TestClass]
    public class RentTypeTests
    {
        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void RentType_InvalidParameters_throws_exception()
        {
            RentType rentType = new RentType(-1, "", -1);
        }

        [TestMethod]
        public void RentType_TestConstructor_ValidParameters()
        {
            RentType rentType = new RentType(1, "Hour", 5);

            Assert.IsTrue(rentType.Id == 1);
            Assert.AreEqual(rentType.Type, "Hour");
            Assert.AreEqual(rentType.Price, 5);

        }



        [TestMethod]
        public void RentType_GetOneByID_NotFound_ReturnNull()
        {
            RentType r1 = new RentType().GetOneById(100);
            Assert.IsTrue(r1 == null);
        }

        [TestMethod]
        public void RentType_GetOneByID_Found_ReturnRentType()
        {
            RentType rentType = new RentType().GetOneById(1);
            Assert.IsInstanceOfType(rentType, typeof(RentType));
        }

        [TestMethod]
        public void RentType_GetAll_ReturnListRentTypes()
        {
            List<RentType> rentTypes = new RentType().GetAll();
            Assert.IsNotNull(rentTypes);
            Assert.IsInstanceOfType(rentTypes, typeof(List<RentType>));
            Assert.IsTrue(rentTypes.Count > 0);

        }
    }
}
