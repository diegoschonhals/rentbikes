﻿using System;
using BusinessRules.Classes;
using BusinessRules.Exceptions;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    [TestClass]
    public class PromotionTests
    {
        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void Promotion_InvalidParameters_throws_exception()
        {
            Promotion p = new Promotion(-1, "", -1);
        }

        [TestMethod]
        public void Promotion_TestConstructor_ValidParameters()
        {
            Promotion p = new Promotion(1, "Familiar", 30);

            Assert.IsTrue(p.Id == 1);
            Assert.AreEqual(p.Type, "Familiar");
            Assert.AreEqual(p.DiscountPercentage, 30);

        }



        [TestMethod]
        public void Promotion_GetOneByID_NotFound_ReturnNull()
        {
            Promotion Promotion = new Promotion();
            Promotion b1 = Promotion.GetOneById(100);
            Assert.IsTrue(b1 == null);
        }

        [TestMethod]
        public void Promotion_GetOneByID_Found_ReturnPromotion()
        {
            Promotion Promotion = new Promotion();
            Promotion b1 = Promotion.GetOneById(1);
            Assert.IsInstanceOfType(b1, typeof(Promotion));
        }

        [TestMethod]
        public void Promotion_GetAll_ReturnListPromotions()
        {
            Promotion Promotion = new Promotion();
            List<Promotion> Promotions = Promotion.GetAll();
            Assert.IsNotNull(Promotions);
            Assert.IsInstanceOfType(Promotions, typeof(List<Promotion>));
            Assert.IsTrue(Promotions.Count > 0);

        }
    }
}
