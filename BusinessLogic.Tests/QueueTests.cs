﻿using System;
using BusinessRules.Classes;
using BusinessRules.Exceptions;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace BusinessLogic.Tests
{
    [TestClass]
    public class QueueTests
    {
        [TestMethod]
        public void Queue_Send_Return_True_IfMessage_IsSent()
        {
            Assert.IsTrue(Queue.SendMessage("myQueue", "Bike was rented.")==true);
        }

        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void Queue_Send_Return_Throw_Exception()
        {
            Queue.SendMessage("", ".");
        }


        [TestMethod]
        public void Queue_Read_Return_List_String()
        {
            bool messageSent = Queue.SendMessage("myQueue", "Bike was rented again");
            List<string> messagesSent = Queue.ReadQueue("myQueue");
            Assert.IsTrue(messagesSent.Count >0 );
        }

        [TestMethod]
        public void Queue_Read_BadQueueName_Return_Null()
        {
            List<string> messagesSent = Queue.ReadQueue("myQueueFalse");
            Assert.IsTrue(messagesSent == null);
        }


    }
}
