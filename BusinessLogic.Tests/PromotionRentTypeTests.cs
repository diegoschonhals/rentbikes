﻿using System;
using BusinessRules.Classes;
using BusinessRules.Exceptions;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    [TestClass]
    public class PromotionRentTypeRentTypeTests
    {
        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void PromotionRentType_InvalidParameters_throws_exception()
        {
            PromotionRentType promotionRentType = new PromotionRentType(0, 0);
        }

        [TestMethod]
        public void PromotionRentType_TestConstructor_ValidParameters()
        {
            PromotionRentType promotionRentType = new PromotionRentType(1, 1);

            Assert.IsTrue(promotionRentType.PromotionId == 1);
            Assert.IsTrue(promotionRentType.RentTypeId == 1);

            Assert.IsTrue(promotionRentType.Promotion != null);
            Assert.IsInstanceOfType(promotionRentType.Promotion, typeof(BusinessRules.Classes.Promotion));

            Assert.IsTrue(promotionRentType.RentType != null);
            Assert.IsInstanceOfType(promotionRentType.RentType, typeof(BusinessRules.Classes.RentType));

        }



        [TestMethod]
        public void PromotionRentType_CalculatePrice_Found_ReturnDecimalGreaterThan0()
        {
            decimal valueFound = new PromotionRentType().CalculatePrice(1,1);
            Assert.IsTrue(valueFound > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void PromotionRentType_CalculatePrice_WrongValues_ReturnException()
        {
            decimal valueFound = new PromotionRentType().CalculatePrice(-10, -10);
         
        }

        [TestMethod]
        public void PromotionRentType_CalculatePrice_NotFoundValues_Return0()
        {
            decimal valueFound = new PromotionRentType().CalculatePrice(10, 10);
            Assert.AreEqual<decimal>(valueFound, 0);
        }



        [TestMethod]
        public void PromotionRentType_GetAll_ReturnListPromotionRentTypes()
        {
            List<PromotionRentType> PromotionRentTypes = new PromotionRentType().GetAll();
            Assert.IsNotNull(PromotionRentTypes);
            Assert.IsInstanceOfType(PromotionRentTypes, typeof(List<PromotionRentType>));
            Assert.IsTrue(PromotionRentTypes.Count > 0);

        }
    }
}
