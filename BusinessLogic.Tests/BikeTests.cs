﻿using System;
using BusinessRules.Classes;
using BusinessRules.Exceptions;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    [TestClass]
    public class BikeTests
    {
        [TestMethod]
        [ExpectedException(typeof(ParametersException))]
        public void Bike_InvalidParameters_throws_exception()
        {
            Bike b = new Bike(-10, "", "", "", 0, "Desc");
        }

        [TestMethod]
        public void Bike_TestConstructor_ValidParameters()
        {
            Bike b = new Bike(1, "GT", "Brand", "RED", 22, "Desc");

            Assert.IsTrue(b.Id == 1);
            Assert.AreEqual(b.Model, "GT");
            Assert.AreEqual(b.Brand, "Brand");
            Assert.AreEqual(b.MainColour, "RED");
            Assert.AreEqual(b.WheelSize, 22);
            Assert.AreEqual(b.Description, "Desc");
        }

       

        [TestMethod]
        public void Bike_GetOneByID_NotFound_ReturnNull()
        { 
            Bike b1 = new Bike().GetOneById(100);
            Assert.IsTrue(b1 == null);
        }

        [TestMethod]
        public void Bike_GetOneByID_Found_ReturnBike()
        {
            Bike b1 = new Bike().GetOneById(1);
            Assert.IsInstanceOfType(b1, typeof(Bike));
        }

        [TestMethod]
        public void Bike_GetAll_ReturnListBikes()
        {
            List<Bike> bikes = new Bike().GetAll();
            Assert.IsNotNull(bikes);
            Assert.IsInstanceOfType(bikes, typeof(List<Bike>));
            Assert.IsTrue(bikes.Count>0);

        }
    }
}
