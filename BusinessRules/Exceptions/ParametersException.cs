﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Exceptions
{
    public class ParametersException : Exception
    {
        public ParametersException(string message)
           : base(message)
        {
            message = "Errors found on parameters. ";
        }

        
        
    }
}
