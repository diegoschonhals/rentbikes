1. DESIGN.
1.1) See RentBikesDiagram in BusinessRules root project 
1.2) RentedBikes table saves all information about RentedBikes and Promotions
1.3) If promotionID is null, no promotion was applied
1.4) Price of the rented bike is calculated based on RentedType and PromotionID, under RentedBike class

2. DEVELOPMENT
2.1) RentedBikes.GetAll() method assume that related classes have information preloaded.
2.2) Linq was used to retrieve data from classes.
2.3) MSMQ queue was used to send and recieve messages queue.

3) TESTING
3.1) Standard Microsoft XUnit was used for testing
3.2) All methods and constructors were added on BusinessRules.Tests project
3.3) For changing test cases, change constant values on test methods.

4) MSMQ Installations
4.1) It's possible you have to install MSMQ from control panel, for further information check this url:
https://www.infoworld.com/article/3060115/how-to-work-with-msmq-in-c.html
4.2) System.Messaging.dll was added into "Libraries" folder, if it's necessary to reference on BusinessRules project
