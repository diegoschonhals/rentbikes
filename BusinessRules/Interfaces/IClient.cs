﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Classes;

namespace BusinessRules.Interfaces
{
    interface IClient
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Phone { get; set; }
        string Address { get; set; }
        string DocumentId { get; set; }
        string Email { get; set; }

        Client GetOneById(int id);
        List<Client> GetAll();
    }
}
