﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Classes;

namespace BusinessRules.Interfaces
{
    interface IPromotion
    {
        int Id { get; set; }
        string Type { get; set; }
        short  DiscountPercentage { get; set; }

        List<Promotion> GetAll();

        Promotion GetOneById(int id);
    }
}
