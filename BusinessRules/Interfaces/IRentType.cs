﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Classes;

namespace BusinessRules.Interfaces
{
    interface IRentType
    {
         int Id { get; set; }
         string Type { get; set; }
         decimal Price { get; set; }

        List<RentType> GetAll();

        RentType GetOneById(int id);

    }
}
