﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Classes;

namespace BusinessRules.Interfaces
{
    interface IRentedBike
    {
        int Id { get; set; }
        int ClientId { get; set; }
        int BikeId { get; set; }
        Nullable<int> RentTypeId { get; set; }
        System.DateTime StartDate { get; set; }
        System.DateTime EndDate { get; set; }
        Nullable<decimal> Price { get; set; }
        Nullable<int> PromotionId { get; set; }


        bool IsBikeRented(int idBike, DateTime startDate, DateTime endDate, List<RentedBike> rentedBikes);
        List<RentedBike> GetAll();
        bool RentBike(int clientId, int bikeId, int rentTypeId, DateTime startDate, DateTime endDate, int? promotionId, ref List<RentedBike> rentedBikes);
    }
}
