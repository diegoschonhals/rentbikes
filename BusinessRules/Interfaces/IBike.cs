﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Classes;

namespace BusinessRules.Interfaces
{
    interface IBike
    {

         int Id { get; set; }

         string Model { get; set; }

         string Brand { get; set; }

         string MainColour { get; set; }

         int WheelSize { get; set; }

         string Description { get; set; }

        Bike GetOneById(int Id);

        List<Bike> GetAll();


           
    }
}
