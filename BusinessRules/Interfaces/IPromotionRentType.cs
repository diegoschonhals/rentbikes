﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Classes;

namespace BusinessRules.Interfaces
{
    interface IPromotionRentType
    {
        int PromotionId { get; set; }
        int RentTypeId { get; set; }

        decimal CalculatePrice(int rentTypeId, int promotionId);

        List<PromotionRentType> GetAll();
    }
}
