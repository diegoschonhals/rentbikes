﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Exceptions;

namespace BusinessRules.Classes
{
    public class RentedBike: BusinessRules.Interfaces.IRentedBike
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int BikeId { get; set; }
        public Nullable<int> RentTypeId { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> PromotionId { get; set; }



        public RentedBike() {

                }

        public RentedBike(int id, int bikeId, int clientId, int rentTypeId, DateTime startDate, DateTime endDate, decimal price, int? promotionId)
        {
            if (id < 1 || bikeId < 1 || clientId < 1 || rentTypeId < 1 || endDate <= startDate || price < 0)
                throw new BusinessRules.Exceptions.ParametersException("Check parameters sent.");

            this.Id = id;
            this.ClientId = clientId;
            this.BikeId = bikeId;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Price = price;
            this.RentTypeId = rentTypeId;
            this.PromotionId = promotionId;
        }

        
        public bool IsBikeRented(int bikeId, DateTime startDate, DateTime endDate, List<RentedBike> rentedBikes)
        {
            List<RentedBike> reducedRentedBikes = rentedBikes.Where(x => x.BikeId == bikeId && ((startDate >= x.StartDate  && startDate <= x.EndDate) || (endDate >= x.StartDate && endDate <= x.EndDate))  ).ToList<RentedBike>();

            if (reducedRentedBikes.Count<RentedBike>() > 0)
                return true;
            else
                return false;


        }

        public int GetNextIdValue(List<RentedBike> rentedBikes)
        {
            if (rentedBikes != null)
                return rentedBikes.Max<RentedBike>(x => x.Id) + 1;
            else
                return 1;
        }

      
        public List<RentedBike> GetAll()
        {
            #region CommentsAndInstances
            //we assume:
            // ClientIds exists: 1,2
            //BikeIds exists: 1,2,3,4,5,6
            //RentTypes exists: 1,2,3
            //Promotions may exists or not, if they exist we have: 1,2
            //If rental is per week, we sum 7 days to referenceDate, otherwise EndDate remains the same until 23:59

            //Create classes instances
            RentType rt = new RentType();
            PromotionRentType prt = new PromotionRentType();
            #endregion

            #region RentedBike1
            DateTime referenceDate1 = DateTime.Now.Date;
            DateTime maxDayDate1 = new DateTime(referenceDate1.Year, referenceDate1.Month, referenceDate1.Day, 23, 59, 59);
            decimal price1 = rt.GetOneById(1).Price;
            RentedBike rb1 = new RentedBike(1, 1, 1, 1, referenceDate1, maxDayDate1, price1, null);
            #endregion


            #region RentedBike2
            DateTime referenceDate2 = DateTime.Now.Date.AddDays(2);
            DateTime maxDayDate2 = new DateTime(referenceDate2.Year, referenceDate1.Month, referenceDate2.Day, 23, 59, 59);
            decimal price2 = rt.GetOneById(2).Price;
            RentedBike rb2 = new RentedBike(1, 2, 2, 1, referenceDate2, maxDayDate2, price2, null);
            #endregion


            #region RentedBike3
            DateTime referenceDate3 = DateTime.Now.Date.AddDays(3);
            DateTime maxDayDate3 = new DateTime(referenceDate3.Year, referenceDate1.Month, referenceDate3.Day, 23, 59, 59);
            decimal price3 = rt.GetOneById(2).Price;
            RentedBike rb3 = new RentedBike(1, 3, 3, 1, referenceDate3, maxDayDate3, price3, null);
            #endregion


            #region RentedBikeWithPromotion_1Week
            //Use week rental with promotion
            DateTime referenceDate4 = DateTime.Now.Date.AddDays(4);
            DateTime maxDayDate4 = new DateTime(referenceDate4.Year, referenceDate4.Month, referenceDate4.Day, 23, 59, 59).AddDays(7);

            RentType rt4 = new RentType();
            decimal price4 = prt.CalculatePrice(1, 3);
            RentedBike rb4 = new RentedBike(1, 4, 4, 1, referenceDate4, maxDayDate4, price4, 1);
            #endregion

            #region ReturnListRentedBikes
            List<RentedBike> rentedBikes = new List<RentedBike>();
            rentedBikes.Add(rb1);
            rentedBikes.Add(rb2);
            rentedBikes.Add(rb3);
            rentedBikes.Add(rb4);

            #endregion

            return rentedBikes;
        }



        public bool RentBike(int clientId, int bikeId, int rentTypeId, DateTime startDate, DateTime endDate, int? promotionId, ref List<RentedBike> rentedBikes)
        {

            if (bikeId < 1 || clientId < 1 || rentTypeId < 1 || endDate <= startDate)
                throw new BusinessRules.Exceptions.ParametersException("Check parameters sent.");

            bool returnValue = false;

            try
            {
                RentedBike rb1 = new RentedBike();
                PromotionRentType prt = new PromotionRentType();

                if (!rb1.IsBikeRented(bikeId, startDate, endDate, rentedBikes))
                {
                    decimal price = 0;
                    if (promotionId != null)
                        price = prt.CalculatePrice(promotionId.Value, rentTypeId);
                    else
                    {
                        RentType rt = new RentType().GetOneById(rentTypeId);

                        if (rt != null)
                            price = rt.GetOneById(rentTypeId).Price;
                    }

                    if (price > 0)
                    {
                        rb1 = new RentedBike(GetNextIdValue(rentedBikes), bikeId, clientId, rentTypeId, startDate, endDate, price, promotionId);
                        rentedBikes.Add(rb1);
                        Queue.SendMessage("myQueue", "Bike was rented.");
                        returnValue = true;
                    }
                    else
                         returnValue = false;

                }
            }
            catch (Exception e)
            {
            }

            return returnValue;
           



        }

    }
}
