﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Classes
{
   public  class Promotion: Interfaces.IPromotion
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public short DiscountPercentage { get; set; }

        public Promotion() { }
        public Promotion(int id, string type, short discount)
        {
            if (id < 1 || string.IsNullOrEmpty(type) || discount<0)
                throw new BusinessRules.Exceptions.ParametersException("Check parameters sent.");


            this.Id = id;
            this.Type = type;
            this.DiscountPercentage = discount;

        }

        public List<Promotion> GetAll()
        {
            Promotion p1 = new Promotion(1, "Familiar", 30);
            Promotion p2 = new Promotion(1, "Winter", 40);
            List<Promotion> promotions = new List<Promotion>();
            promotions.Add(p1);
            promotions.Add(p2);
            return promotions;

        }

        public Promotion GetOneById(int id)
        {
            if (this.GetAll().Where<Promotion>(x => x.Id == id).Count<Promotion>() > 0)
                return this.GetAll().Where<Promotion>(x => x.Id == id).First<Promotion>();
            else
                return null;
        }
    }
}
