﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Interfaces;

namespace BusinessRules.Classes
{
    public class RentType : IRentType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }

        public RentType() { }
        public RentType(int id, string type, decimal price)
        {

            if (id < 1 || string.IsNullOrEmpty(type) || price < 0)
                throw new BusinessRules.Exceptions.ParametersException("Check parameters sent.");

            this.Id = id;
            this.Type = type;
            this.Price = price;
            
        }
        public List<RentType> GetAll()
        {
            /**
             *  1. Rental by hour, charging $5 per hour
                2. Rental by day, charging $20 a day
                3. Rental by week, changing $60 a week
             */

            RentType rt1 = new RentType(1, "hour", 5);
            RentType rt2 = new RentType(2, "Day", 20);
            RentType rt3 = new RentType(3, "hour", 60);

            List<RentType> rentTypes = new List<RentType>();
            rentTypes.Add(rt1);
            rentTypes.Add(rt2);
            rentTypes.Add(rt3);
            return rentTypes;
        }

        public RentType GetOneById(int id)
        {
            if (this.GetAll().Where<RentType>(x => x.Id == id).Count<RentType>() > 0)
                return this.GetAll().Where<RentType>(x => x.Id == id).First<RentType>();
            else
                return null;
        }
    }
}
