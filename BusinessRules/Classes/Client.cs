﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Classes
{
    public class Client : BusinessRules.Interfaces.IClient
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string DocumentId { get; set; }
        public string Email { get; set; }

        public Client() { }
        public Client(int id, string firstName, string lastName, string phone, string address, string documentId, string email)
        {
            if (id < 1 || string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                throw new BusinessRules.Exceptions.ParametersException("Check parameters sent.");

            this.Id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Phone = phone;
            this.Address = address;
            this.DocumentId = documentId;
            this.Email = email;

        }

        public Client GetOneById(int id)
        {
            if (this.GetAll().Where<Client>(x => x.Id == id).Count<Client>() > 0)
                return this.GetAll().Where<Client>(x => x.Id == id).First<Client>();
            else
                return null;
        }

        public List<Client> GetAll()
        {
            Client c1 = new Client(1, "Diego", "Schonhals", "3446-650079", "Primera Junta 7, Gchu", "18817829", "dschonhals@hotmail.com");
            Client c2 = new Client(2, "Gabriela", "Urrels", "3446-529381", "San Jose 780, Gchu", "2532829", "gabrielaurrels@hotmail.com");
            Client c3 = new Client(3, "Cristina", "Kirchner", "011-4256-529381", "San Juan 22, Rio Gallegos", "2532829", "cfk@hotmail.com");
            List<Client> clients = new List<Client>();
            clients.Add(c1);
            clients.Add(c2);
            clients.Add(c3);
            return clients;
        }
    }
}

