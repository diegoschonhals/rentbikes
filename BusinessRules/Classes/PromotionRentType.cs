﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessRules.Interfaces;

namespace BusinessRules.Classes
{
    public class PromotionRentType: IPromotionRentType
    {
        public int PromotionId { get; set; }
        public int RentTypeId { get; set; }

        public Promotion Promotion { get; set; }
        public RentType RentType { get; set; }

        public PromotionRentType() { }

        public PromotionRentType(int promotionId, int rentTypeId)
        {
            if (promotionId < 1 || rentTypeId < 1)
                throw new BusinessRules.Exceptions.ParametersException("Check parameters sent.");

            this.PromotionId = promotionId;
            this.RentTypeId = rentTypeId;

            Promotion p = new Promotion();
            this.Promotion = p.GetOneById(promotionId);

            RentType rt = new RentType();
            this.RentType = rt.GetOneById(rentTypeId);

        }

        public decimal CalculatePrice(int promotionId, int rentTypeId)
        {
            PromotionRentType prt = new PromotionRentType(promotionId, rentTypeId);

            if (prt.RentType == null || prt.Promotion == null)
                return 0;
            else
                return prt.RentType.Price - ((prt.RentType.Price * prt.Promotion.DiscountPercentage) / 100);
        }


        public List<PromotionRentType> GetAll()
        {
            PromotionRentType prt1 = new PromotionRentType(1, 1);
            PromotionRentType prt2 = new PromotionRentType(1, 2);
            PromotionRentType prt3 = new PromotionRentType(1, 3);

            List<PromotionRentType> promotionRentTypes = new List<PromotionRentType>();
            promotionRentTypes.Add(prt1);
            promotionRentTypes.Add(prt2);
            promotionRentTypes.Add(prt3);

            return promotionRentTypes;
        }

    }

  
   
}
