﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Messaging;
using BusinessRules.Exceptions;

namespace BusinessRules.Classes
{
    public static class Queue
    {
        //info class details: https://www.infoworld.com/article/3060115/how-to-work-with-msmq-in-c.html


        public static bool SendMessage(string queue, string message)
        {
            if (string.IsNullOrEmpty(queue) || string.IsNullOrEmpty(message))
                throw new ParametersException("Fill queue name and message to be sent.");


            string pathQueue = ".\\Private$\\" + queue;
            try
            {

                MessageQueue messageQueue;
                if (MessageQueue.Exists(pathQueue))
                {

                    messageQueue = new MessageQueue(pathQueue);
                    messageQueue.Label = "Rent Bike System";

                }
                else
                {

                    MessageQueue.Create(pathQueue);
                    messageQueue = new MessageQueue(pathQueue);
                    messageQueue.Label = "Rent Bike System";

                }

                messageQueue.Send(message);
                return true;

            }
            catch (Exception ex)
            {

                    return false;

             }
        }

        public static List<string> ReadQueue(string queue)
        {
            string pathQueue = ".\\Private$\\" + queue;
            List<string> lstMessages = new List<string>();

            try
            {
                using (MessageQueue messageQueue = new MessageQueue(pathQueue))
                {

                    System.Messaging.Message[] messages = messageQueue.GetAllMessages();

                    foreach (System.Messaging.Message message in messages)
                    {

                        message.Formatter = new XmlMessageFormatter(
                        new String[] { "System.String, mscorlib" });
                        string msg = message.Body.ToString();
                        lstMessages.Add(msg);

                    }

                }
            }
            catch(Exception ex)
            {
                return null;
            }
           

            return lstMessages;

        }


    }
}
