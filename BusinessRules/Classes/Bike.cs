﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Classes
{
    public class Bike : BusinessRules.Interfaces.IBike
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public string MainColour { get; set; }
        public int WheelSize { get; set; }
        public string Description { get; set; }

        public Bike() { }

        public Bike(int id, string model, string brand, string mainColour, int wheelSize, string description)
        {
            if (id < 0 || string.IsNullOrEmpty(model) || string.IsNullOrEmpty(brand) || string.IsNullOrEmpty(mainColour) || wheelSize < 1)
                throw new BusinessRules.Exceptions.ParametersException("Check parameters sent.");

            this.Id = id;
            this.Model = model;
            this.Brand = brand;
            this.MainColour = mainColour;
            this.WheelSize = wheelSize;
            this.Description = description;
        }
        
        public Bike GetOneById(int id)
        {
            if (this.GetAll().Where<Bike>(x => x.Id == id).Count<Bike>() > 0)
                return this.GetAll().Where<Bike>(x => x.Id == id).First<Bike>();
            else
                return null;
        }

        public List<Bike> GetAll()
        {
            Bike b1 = new Bike(1, "Race", "GT", "Red", 26, "for race");
            Bike b2 = new Bike(2, "Race", "Venzo", "Blue", 26, "for race");
            Bike b3 = new Bike(3, "Ride", "GT", "Black", 22, "To ride");
            Bike b4 = new Bike(4, "Ride", "Venzo", "White", 26, "to ride");
            Bike b5 = new Bike(5, "Children", "GT", "Yellow", 16, "for children");
            Bike b6 = new Bike(6, "Children", "Venzo", "Green", 16, "for children");

            List<Bike> bikes = new List<Bike>();
            bikes.Add(b1);
            bikes.Add(b2);
            bikes.Add(b3);
            bikes.Add(b4);
            bikes.Add(b5);
            bikes.Add(b6);

            return bikes;
        }
    }
}
